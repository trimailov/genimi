genimi: src/genimi.nim
	nimble -d:ssl build

run: genimi
	./genimi gemini://gemini.circumlunar.space/

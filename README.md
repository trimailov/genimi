# genimi

A [Gemini protocol](https://gemini.circumlunar.space/) client written in [nim](https://nim-lang.org/)

## Use

```
genimi <url>
```

## Build and run

```
make run
```

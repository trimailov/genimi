import net
import os
import strformat
import strutils
import uri


proc get(url: string, port: int = 1965): string =
  let uri = parseUri(url)
  var socket = newSocket()

  # XXX: does not verify actual TLS certificates
  var sslctx = newContext(verifyMode = CVerifyNone)
  sslctx.wrapSocket(socket)

  socket.connect(uri.hostname, Port(port))
  let data = &"{uri}\r\n"
  socket.send(data)

  var resp: string = ""

  while true:
    var line = socket.recvLine()
    if line != "":
      # strip line end characters if they exist, to not add duplicate new line
      line.stripLineEnd()
      resp = &"{resp}{line}\n"
    else:
      break

  socket.close()
  sslctx.destroyContext()

  return resp


when isMainModule:
  let url = paramStr(1)
  let resp = get(url)
  echo resp

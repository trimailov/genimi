# Package

version       = "0.1.0"
author        = "Justas Trimailovas"
description   = "Gemini protocol client"
license       = "MIT"
srcDir        = "src"
bin           = @["genimi"]



# Dependencies

requires "nim >= 1.2.6"
